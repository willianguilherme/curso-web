import { ProductService } from './../product.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { Product } from './../product.model';


@Component({
  selector: 'app-product-delete',
  templateUrl: './product-delete.component.html',
  styleUrls: ['./product-delete.component.css']
})
export class ProductDeleteComponent implements OnInit {

  product: Product | any

  constructor(private router: Router, private productService: ProductService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id')
    this.productService.readById(id).subscribe(produto => {
      this.product = produto
    })
  }

  deleteProduct():void {
    const name = this.product.name
    this.productService.delete(this.product.id).subscribe(() => {
      this.productService.showMessage(`O produto ${name} foi excluido`)
    })
    this.router.navigate(['/products'])
  }
  cancel(): void {
    this.router.navigate(['/products'])

  }

}
