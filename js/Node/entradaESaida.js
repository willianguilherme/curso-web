const anonimo = process.argv.indexOf('-a') !== -1
console.log(anonimo)

if(anonimo) {
    process.stdout.write('Fala anonimo\n')
    process.exit() // encerra process
}
else {
    process.stdout.write('Informa seu nome: ')
    process.stdin.on('data', data => {      //'data é um tipo  - -  le do teclado e recebe quando apertar enter  --  no fim da string fica um \n
        const nome = data.toString().replace('\n','')
        process.stdout.write(`fala ${nome}\n`)
        process.exit()
    })
}