//npm i node-schedule
const schedule = require('node-schedule')                                  // (*/x de x em x segundos)
                                                                      // (x aos x segundos)
const tarefa1 = schedule.scheduleJob(('*/5 * 10 * * 2'), function () {// (*/deQuantosSegundos minutos hora qualquerMes qualquerDiaDoMes diaDaSemana(domingo(0)))
    console.log("executando tarefa 1! ", new Date().getSeconds())
})

setTimeout(function () {
    tarefa1.cancel()
    console.log('Cancelando tarefa1')
}, 20000)

const regra = new schedule.RecurrenceRule()
regra.dayOfWeek = [new schedule.Range(1, 5)]
regra.hours = 12
regra.seconds = 30

const tarefa2 = schedule.scheduleJob(regra, function () {
    console.log('executando tarefa 2!', new Date().getSeconds())
})

//setInterval
//setImmediate