const porta = 3003

const express = require('express')
const app = express()
const bdd = require('./bancoDeDados')
const bodyParser = require('body-parser')

//app.use((req, resp, next) => { se usar assim, sem a url e com o uso ele fica "global", atendendo todas as requisições
app.use(bodyParser.urlencoded({extended: true})) // se os dados da requisição estiverem no padrao urlencoded ele usa essa função, transformando os dados em objeto

app.get('/produtos', (req, res, next) => {
    res.send(bdd.getProdutos())                 // o metodo send converte para json
})

app.get('/produtos/:id', (req, res, next) => {  // /produtos/id , id é um numeros, : indica que né um paramentro
    res.send(bdd.getProduto(req.params.id))
})

app.post('/produtos', (req, res, next) => {
    const produto = bdd.salvarProduto({
        nome: req.body.nome,                    //req.body acessa os dados que entraram a partir de um post
        preco: req.body.preco
    })
    res.send(produto)                           // converte para json
})

app.put('/produtos/:id', (req, res,next) => {
    const produto = bdd.salvarProduto({
        id: req.params.id,
        nome: req.body.nome,
        preco: req.body.preco
    })
    res.send(produto)
})

app.delete('/produtos/:id', (req, res,next) => {
    const produto = bdd.excluirProduto(req.params.id)
    res.send(produto)
})

app.listen(porta, () => {
    console.log(`servidor executando na porta ${porta}`)
})

// npm i --save body-parser@1.18.2 -E -- body-parser permite entrega os dados da requisição no req.body