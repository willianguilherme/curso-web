function gerarNumeros(min, max, tempo) {
    if(min > max) {
        [max, min] = [min, max]
    }
    return new Promise(resolve => {
        setTimeout(function() {
            const aleatorio = parseInt(Math.random() * (max - min + 1)) + min
            resolve(aleatorio)
        }, tempo)
        
    } )
}

function gerarVariosNumeros() {
    return Promise.all([
        gerarNumeros(1,50, 1000),
        gerarNumeros(1,50, 2000),
        gerarNumeros(1,50, 3000),
        gerarNumeros(1,50, 100),
        gerarNumeros(1,50, 3000),
        gerarNumeros(1,50, 10000),

    ])
}

gerarVariosNumeros().then(console.log)