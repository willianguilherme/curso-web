//Object.values Object.entries

const obj = {a: 3, b:4, c: 5}
console.log(Object.values(obj))
console.log(Object.entries(obj))

const tag = function(partes, ...valores) {
    console.log(partes)
    console.log(valores)
    return `qualquer`
}

const aluno = 'Gui'
const situacao = 'aprovado'

console.log(tag `${aluno} esta ${situacao}`) //partes recebe a string e valores as variaveis