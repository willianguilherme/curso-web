const tecnologia = new Map()
tecnologia.set('react', { framework: false})
tecnologia.set('angular', {framework: true})

console.log(tecnologia.react) //indefinido
console.log(tecnologia.get('react'))

const chavesVariadas = new Map([
    [function () { }, 'funcao'],
    [{}, 'Objeto'],
    [123, 'Numero'],
])

chavesVariadas.forEach((vl, ch) => {
    console.log(ch, vl)
})
console.log(chavesVariadas)
chavesVariadas.delete(123)
console.log(chavesVariadas.has(123))
console.log(chavesVariadas.size)