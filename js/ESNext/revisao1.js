//destructuring
const [l,e,...tras] = 'Cod3r' //l = c, e = o, tras[] = d3r
console.log(l,e, tras)

const [x, , y] = [1, 2, 3]
console.log(x, y)