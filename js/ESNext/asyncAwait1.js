function esperarPor(tempo = 2000) {
    return new Promise(function (resolve) {
        setTimeout(function() {
            console.log('Executando promise ...')
            resolve()
        }, tempo)
    })
}

//esperarPor()
//    .then(esperarPor)
//    .then(esperarPor)
//    .then(esperarPor)

async function executar() {
    await esperarPor()              // sem o await ele executa de forma assincrona
    console.log('async await 1')
    await esperarPor()
    console.log('async await 2')
    await esperarPor()
    console.log('async await 3')

}

executar()