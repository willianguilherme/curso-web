function funcionaOuNao(valor, chanceErro) {
    return new Promise((resolve, reject) => {
        try {
            if(Math.random() > chanceErro)
                reject('Ocorreu um erro')
            else
                resolve(valor)
        } catch (e) {
            reject(e)
        }
    })
}

funcionaOuNao('Testando', 0.5)
    .then(console.log)
    .catch(err => console.log(`Erro2: ${err}`))
    .then(() => console.log('Fim!')) // depois do console.log não é retornado nada logo () sem parametros
    // quando ha erro, o primeiro tratamento encontrado pe executado