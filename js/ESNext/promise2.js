/*setTimeout(function() {
    console.log('Executando funcao ...')

    setTimeout(function() {
        console.log('Executando funcao ...')
    
        setTimeout(function() {
            console.log('Executando funcao ...')
        
            
        }, 2000)
    }, 2000)    
}, 2000)*/

function esperarPor(tempo = 2000) {
    return new Promise (function(resolve) {
        setTimeout(function() {
            console.log('Executando promise ')
            resolve()   // quando executa o resolve o then é chamado
        }, 2000)
    })
}

esperarPor(3000)
    .then(esperarPor(3000))
    .then(esperarPor)