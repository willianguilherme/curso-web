//processamento assincrono
function falaDepoisDe(segundos, frase) {
    return new Promise((resolve, reject) => { // usamos o nome de resolve para a parte que resolve a função
        setTimeout(() => {
            resolve(frase) // vai ser resolvido peleo then
        }, segundos * 1000);
    })
}

falaDepoisDe(3, 'ola meu caro')
.then(frase => frase.concat(' eaasd')) // o valor trablhado aqui é passado para o proximo then
//.then(v => console.log(v)) são equivalentes
.then(console.log) // não é necessario passar () para as funções, pois o then sempre retorna 1 elemento
.catch(e => console.log(e))         //ira ser executado caso aconteça algum erro nos the