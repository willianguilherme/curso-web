const fs = require('fs')

function leArquivo(caminho) {
    return new Promise(resolve => {
        let texto = fs.readFile(caminho, (_,valor) => {   // deve se passar dois parametros (_, valor) pois op primeiro recebera um possivel erro
            //console.log(valor.toString())
            resolve(valor.toString())
        })
    })
}

leArquivo(__dirname+'/textoDesafioPromise.txt')
    .then(console.log)