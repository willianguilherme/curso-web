// nao aceita repetição e nao é indexada
const times = new Set() 
times.add('Corinthians')
times.add('Santos')
times.add('São paulo')
times.add('coritiba')
times.add('Santos')

console.log(times)
console.log(times.size)
console.log(times.has('Santos'))
times.delete('Santos')
console.log(times)

const nomes = ['Willian', 'Guilherme', 'Souza', 'Willian']
const nomeSet = new Set(nomes)
console.log(nomeSet)