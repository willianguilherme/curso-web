function gerarNumeros(min, max, numerosProibidos) {
    if(min > max) {
        [max, min] = [min, max]
    }
    return new Promise((resolve, reject) =>{
        const aleatorio = parseInt(Math.random() * (max - min + 1)) + min
        if(numerosProibidos.includes(aleatorio)){
            reject('Numero Repetido')
        }
        resolve(aleatorio)
    } )
}

async function gerarMegaSena (qntdNumeros) {
    const numeros = []
    try{

        for(let _ of Array(qntdNumeros).fill()) {
            numeros.push(await gerarNumeros(1, 60, numeros))
        }
        return numeros
    } catch (e) {
        throw 'Deu erro!!'
    }
}

gerarMegaSena(10)
    .then(console.log)
    .catch(console.log)